const jsonServer = require("json-server");
const server = jsonServer.create();
const path = require("path");
const router = jsonServer.router(path.join(__dirname, "db.json"));
const middlewares = jsonServer.defaults({ watch: true });
const low = require("lowdb");
const FileSync = require("lowdb/adapters/FileSync");

const adapter = new FileSync(path.join(__dirname, "db.json"));
const db = low(adapter);

// Set default middlewares (logger, static, cors and no-cache)
server.use(middlewares);

function validateEmail(email) {
  let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use(jsonServer.bodyParser);
server.use((req, res, next) => {
  if (req.method === "POST") {
    let {
      id,
      username,
      email,
      numberOfBookings,
      isPremiumUser,
      premiumCode
    } = req.body;
    let user = db
      .get("users")
      .find({ id })
      .value();
    if (user) {
      if (username.length < 3) {
        return res.status(500).jsonp({
          success: false,
          message: "Error",
          errors: {
            username: "Invalide Username"
          }
        });
      } else if (!validateEmail(email)) {
        return res.status(500).jsonp({
          success: false,
          message: "Error",
          errors: {
            email: "Invalide Email"
          }
        });
      } else if (
        !(Number.isInteger(numberOfBookings) && numberOfBookings >= 0)
      ) {
        return res.status(500).jsonp({
          success: false,
          message: "Error",
          errors: {
            numberOfBookings: "Invalide numberOfBookings"
          }
        });
      } else {
        db
          .get("users")
          .find({ id })
          .assign({
            username,
            email,
            numberOfBookings,
            isPremiumUser,
            premiumCode
          })
          .write();
        return res.status(200).jsonp({
          success: true,
          message: "Updated Successfully"
        });
      }
    } else {
      return res.status(500).jsonp({
        success: false,
        message: "Error",
        errors: {
          id: "Invalid Id"
        }
      });
    }
  }
  // Continue to JSON Server router
  next();
});

// Use default router
server.use(router);
server.listen(4000, () => {
  console.log("JSON Server is running Port: 4000");
});
