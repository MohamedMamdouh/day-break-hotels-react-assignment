import { fromJS } from "immutable";
import { LOAD_USERS, LOAD_USERS_SUCCESS, LOAD_USERS_ERROR } from "./constants";

// The initial state of the App
const initialState = fromJS({
  users: []
});

function users(state = initialState, action) {
  switch (action.type) {
    case LOAD_USERS_SUCCESS:
      let { users } = action;
      return state.set("users", users);
    default:
      return state;
  }
}

export default users;
