/**
 * Homepage selectors
 */

import { createSelector } from "reselect";

const selectHome = state => state.get("home");

const makeSelectUsers = () =>
  createSelector(selectHome, homeState => homeState.get("users"));

export { selectHome, makeSelectUsers };
