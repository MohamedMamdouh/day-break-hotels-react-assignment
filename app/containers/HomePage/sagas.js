import {
  take,
  call,
  put,
  select,
  cancel,
  takeLatest
} from "redux-saga/effects";
import { LOAD_USERS } from "containers/HomePage/constants";

import { usersLoaded, usersLoadingError } from "containers/HomePage/actions";

import request from "utils/request";

export function* getUsers() {
  const requestURL = "http://localhost:3004/users";

  try {
    const users = yield call(request, requestURL);
    yield put(usersLoaded(users));
  } catch (err) {
    yield put(usersLoadingError(err));
  }
}

export function* rootSaga() {
  yield takeLatest(LOAD_USERS, getUsers);
}

export default [rootSaga];
