/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from "react";
import messages from "./messages";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import { loadUsers } from "./actions";
import UsersList from "../../components/UsersList";
import { makeSelectUsers } from "./selectors";
import PropTypes from "prop-types";

class HomePage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    this.props.onLoad();
  }
  render() {
    return (
      <div>
        <h1>Home Page</h1>
        <UsersList users={this.props.users} />
      </div>
    );
  }
}

HomePage.propTypes = {
  users: PropTypes.array,
  onLoad: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    onLoad: () => {
      dispatch(loadUsers());
    }
  };
}

const mapStateToProps = createStructuredSelector({
  users: makeSelectUsers()
});

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
