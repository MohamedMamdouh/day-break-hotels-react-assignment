/*
 * UserProfilePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from "react";
import { connect } from "react-redux";
import { createStructuredSelector } from "reselect";
import UserForm from "../../components/UserForm";
import { getUser, setUserData, clearUserData } from "./actions";
import {
  makeSelectUser,
  makeSelectSuccess,
  makeSelectError
} from "./selectors";
import PropTypes from "prop-types";

class UserProfilePage extends React.PureComponent {
  // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);

    this.state = {
      id: "",
      user: {},
      loading: false,
      success: {},
      error: {}
    };
    this.saveUser = this.saveUser.bind(this);
    this.updateUserState = this.updateUserState.bind(this);
  }

  saveUser(event) {
    event.preventDefault();
    this.props.saveUser(this.state.user);
  }

  componentDidMount() {
    this.props.getUser(this.props.params.id);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.user.id != nextProps.user.id) {
      this.setState({ user: nextProps.user });
    }
    if (this.props.error != nextProps.error) {
      this.setState({ error: nextProps.error.errors });
    }
    if (nextProps.success.success) {
      setTimeout(() => {
        this.context.router.push("/");
      }, 1000);
    }
  }

  updateUserState(event) {
    const field = event.target.name;
    let user = Object.assign({}, this.state.user);
    field == "numberOfBookings"
      ? (user[field] = parseInt(event.target.value))
      : (user[field] =
          event.target.type === "checkbox"
            ? event.target.checked
            : event.target.value);
    return this.setState({ user });
  }

  componentWillMount() {
    this.props.clearState();
  }

  render() {
    return (
      <div>
        <h1>User Profile Page</h1>
        {this.props.success.success ? <h2>Saved successfuly</h2> : null}
        <UserForm
          user={this.state.user}
          onChange={this.updateUserState}
          onSave={this.saveUser}
          errors={this.state.error}
          loading={this.state.loading}
        />
      </div>
    );
  }
}

UserProfilePage.contextTypes = {
  router: PropTypes.object
};

UserProfilePage.propTypes = {
  user: PropTypes.object,
  success: PropTypes.object,
  error: PropTypes.object,
  getUser: PropTypes.func,
  saveUser: PropTypes.func,
  clearState: PropTypes.func
};

export function mapDispatchToProps(dispatch) {
  return {
    getUser: id => {
      dispatch(getUser(id));
    },
    saveUser: user => {
      dispatch(setUserData(user));
    },
    clearState: () => {
      dispatch(clearUserData());
    }
  };
}

const mapStateToProps = createStructuredSelector({
  user: makeSelectUser(),
  success: makeSelectSuccess(),
  error: makeSelectError()
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfilePage);
