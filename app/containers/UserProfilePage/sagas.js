import {
  take,
  call,
  put,
  select,
  cancel,
  takeLatest
} from "redux-saga/effects";

import { GET_USER, SET_USER_DATA } from "containers/UserProfilePage/constants";

import {
  getUserData,
  setUserSuccess,
  setUserError
} from "containers/UserProfilePage/actions";

import axios from "axios";

export function* getUser({ id }) {
  const requestedURL = `http://localhost:3004/users/${id}`;
  try {
    const user = yield call(axios.get, requestedURL);
    yield put(getUserData(user.data));
  } catch (err) {
    yield put(setUserError(err));
  }
}

function* setUserData({ user }) {
  const requestedURL = "http://localhost:4000/users";
  try {
    const userDataSet = yield call(axios.post, requestedURL, user);
    yield put(setUserSuccess(userDataSet.data));
  } catch (error) {
    yield put(setUserError(error.response.data));
  }
}

export function* rootSaga() {
  yield takeLatest(GET_USER, getUser);
  yield takeLatest(SET_USER_DATA, setUserData);
}

export default [rootSaga];
