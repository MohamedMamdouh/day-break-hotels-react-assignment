/*
 * UserProfilePageConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT'
 */

export const GET_USER = "dayBreakHotels/UserProfilePage/GET_USER";
export const GET_USER_DATA = "dayBreakHotels/UserProfilePage/GET_USER_DATA";
export const SET_USER_DATA = "dayBreakHotels/UserProfilePage/SET_USER_DATA";
export const SET_USER_SUCCESS =
  "dayBreakHotels/UserProfilePage/SET_USER_SUCCESS";
export const SET_USER_ERROR = "dayBreakHotels/UserProfilePage/SET_USER_ERROR";
export const CLEAR_USER_DATA = "dayBreakHotels/UserProfilePage/CLEAR_USER_DATA";
