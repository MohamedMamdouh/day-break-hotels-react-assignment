/**
 * Homepage selectors
 */

import { createSelector } from "reselect";

const selectUserProfile = state => state.get("user-profile");

const makeSelectUser = () =>
  createSelector(selectUserProfile, userState => userState.get("user"));

const makeSelectSuccess = success =>
  createSelector(selectUserProfile, userState => userState.get("success"));

const makeSelectError = () =>
  createSelector(selectUserProfile, userState => userState.get("error"));

export {
  selectUserProfile,
  makeSelectUser,
  makeSelectError,
  makeSelectSuccess
};
