import { fromJS } from "immutable";
import {
  GET_USER,
  GET_USER_DATA,
  SET_USER_DATA,
  SET_USER_SUCCESS,
  SET_USER_ERROR,
  CLEAR_USER_DATA
} from "./constants";

// The initial state of the App
const initialState = fromJS({
  user: {},
  success: {},
  error: {}
});

function user(state = initialState, action) {
  switch (action.type) {
    case GET_USER_DATA:
      let { user } = action;
      return state.set("user", user);
    case SET_USER_SUCCESS:
      let { success } = action;
      return state.set("success", success);
    case SET_USER_ERROR:
      let { error } = action;
      return state.set("error", error);
    case CLEAR_USER_DATA:
      return state.merge({
        user: {},
        success: {},
        error: {}
      });
    default:
      return state;
  }
}

export default user;
