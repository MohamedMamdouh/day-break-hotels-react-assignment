import {
  GET_USER,
  GET_USER_DATA,
  SET_USER_DATA,
  SET_USER_SUCCESS,
  SET_USER_ERROR,
  CLEAR_USER_DATA
} from "./constants";

export function getUser(id) {
  return {
    type: GET_USER,
    id
  };
}

export function getUserData(user) {
  return {
    type: GET_USER_DATA,
    user
  };
}

export function setUserData(user) {
  return {
    type: SET_USER_DATA,
    user
  };
}

export function setUserSuccess(success) {
  return {
    type: SET_USER_SUCCESS,
    success
  };
}

export function setUserError(error) {
  return {
    type: SET_USER_ERROR,
    error
  };
}

export function clearUserData() {
  return {
    type: CLEAR_USER_DATA
  };
}
