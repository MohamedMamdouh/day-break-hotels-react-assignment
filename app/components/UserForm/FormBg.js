import styled from "styled-components";

const FormBg = styled.div`
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 20px;
`;

export default FormBg;
