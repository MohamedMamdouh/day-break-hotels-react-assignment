import React from "react";
import TextInput from "./TextInput";
import Input from "./Input";
import FormBg from "./FormBg";

const UserForm = ({ user, onSave, onChange, loading, errors }) => {
  return (
    <FormBg>
      <form>
        <TextInput
          name="username"
          label="Username"
          value={user.username}
          onChange={onChange}
          error={errors.username}
        />

        <TextInput
          name="email"
          label="Email"
          value={user.email}
          onChange={onChange}
          error={errors.email}
        />

        <TextInput
          name="numberOfBookings"
          label="Number of Booking"
          value={user.numberOfBookings}
          onChange={onChange}
          error={errors.numberOfBookings}
        />
        <TextInput
          name="premiumCode"
          label="Premium Code"
          disabled={!user.isPremiumUser}
          value={user.premiumCode}
          onChange={onChange}
          error={errors.premiumCode}
        />

        <label>
          <input
            name="isPremiumUser"
            type="checkbox"
            checked={user.isPremiumUser}
            onChange={onChange}
          />
          Is Premium
        </label>

        <Input
          type="submit"
          disabled={loading}
          value={loading ? "saving..." : "save"}
          className="btn btn-primary"
          onClick={onSave}
        />
      </form>
    </FormBg>
  );
};

export default UserForm;
