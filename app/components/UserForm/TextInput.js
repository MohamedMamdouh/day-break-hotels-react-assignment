import React from "react";
import ErrorMessage from "./ErrorMessage";
import FormTxtInput from "./FormTxtInput";
import styled from "styled-components";

const TextInput = ({
  name,
  label,
  onChange,
  placeholder,
  value,
  error,
  disabled
}) => {
  let wrapperClass = "form-group";
  if (error && error.length > 0) {
    wrapperClass += " " + "has-error";
  }

  return (
    <div className={wrapperClass}>
      <label htmlFor={name}>{label}</label>
      <div className="field">
        {error && <ErrorMessage>{error}</ErrorMessage>}
        <FormTxtInput
          type="text"
          name={name}
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          disabled={disabled}
        />
      </div>
    </div>
  );
};

export default TextInput;
