import React from "react";
import { Link } from "react-router";
import UserItem from "./UserItem";
const UsersListRow = ({ user }) => {
  return (
    <tr>
      <td>
        <Link
          to={"/user-profile/" + user.id}
          style={{ textDecoration: "none" }}
        >
          <UserItem>{user.username}</UserItem>
        </Link>
      </td>
    </tr>
  );
};

export default UsersListRow;
