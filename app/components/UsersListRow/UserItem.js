import styled from "styled-components";

const UserItem = styled.div`
  width: 100%;
  background-color: #4caf50;
  color: white;
  padding: 14px 20px;
  margin: 8px 0;
  border: none;
  border-radius: 4px;
`;

export default UserItem;
