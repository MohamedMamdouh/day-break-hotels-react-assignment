import React, { Component } from "react";
import UsersListRow from "../UsersListRow";

const UsersList = ({ users }) => {
  return (
    <table style={{ width: "100%" }}>
      <thead>
        <tr>
          <th>Username</th>
        </tr>
      </thead>
      <tbody>
        {users.map(user => <UsersListRow key={user.id} user={user} />)}
      </tbody>
    </table>
  );
};

export default UsersList;
